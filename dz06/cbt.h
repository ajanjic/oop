#ifndef COMPLETEBINARYTREE
#define COMPLETEBINARYTREE

#include <iostream>
#include <cassert>
#include <utility>
#include <vector>

typedef unsigned int Node;

template <typename K, typename V>
struct CompleteBinaryTree
{
    std::vector<std::pair<K, V>> N;
    unsigned int size;

    CompleteBinaryTree();
    CompleteBinaryTree(const std::vector<std::pair<K, V>>& L);

    Node left(Node i) const;
    Node right(Node i) const;
    Node parent(Node i) const;

    void updateLeft(Node i, const std::pair<K, V>& e);
    void updateRight(Node i, const std::pair<K, V>& e);
    void updateParent(Node i, const std::pair<K, V>& e);

    bool isLeaf(Node i) const;

    unsigned int getSize();
    void setSize(unsigned int newSize);
    bool empty() const;

    void preorderPrint(Node) const;
    void postorderPrint(Node) const;

    void swapNodes(Node i, Node j);

    const std::pair<K, V>& operator[](int pos) const;
    std::pair<K, V>& operator[](int pos);
    std::pair<K, V>& element(Node i);
    const std::pair<K, V>& element(Node i) const;
};
#include "cbt_impl.h"

#endif
template <typename K, typename V>
CompleteBinaryTree<K, V>::CompleteBinaryTree() : N{}, size{0} {}

template <typename K, typename V>
CompleteBinaryTree<K, V>::CompleteBinaryTree(const std::vector<std::pair<K, V>>& L) : N{L}, size{L.size()}
{}

template <typename K, typename V>
Node CompleteBinaryTree<K, V>::left(Node i) const
{
    return (i * 2 + 1);
}

template <typename K, typename V>
Node CompleteBinaryTree<K, V>::right(Node i) const
{
    return ((i + 1) * 2);
}

template <typename K, typename V>
Node CompleteBinaryTree<K, V>::parent(Node i) const
{
    if(i == 0) return 0;
    return ((i - 1) / 2);
}

template <typename K, typename V>
void CompleteBinaryTree<K, V>::updateLeft(Node i, const std::pair<K, V>& e)
{
    N[left(i)] = e;
}

template <typename K, typename V>
void CompleteBinaryTree<K, V>::updateRight(Node i, const std::pair<K, V>& e)
{
    N[right(i)] = e;
}

template <typename K, typename V>
void CompleteBinaryTree<K, V>::updateParent(Node i, const std::pair<K, V>& e)
{
    assert(i != 0 && "ERR: trying to get parent of root.");
    N[parent(i)] = e;
}

template <typename K, typename V>
bool CompleteBinaryTree<K, V>::isLeaf(Node i) const
{
    return left(i) >= size;
}

template <typename K, typename V>
unsigned int CompleteBinaryTree<K, V>::getSize()
{
    return size;
}

template <typename K, typename V>
void CompleteBinaryTree<K, V>::setSize(unsigned int newSize)
{
    size = newSize;
}

template <typename K, typename V>
bool CompleteBinaryTree<K, V>::empty() const
{
    return (size == 0);
}

template <typename K, typename V>
std::ostream& operator<<(std::ostream& os, const std::pair<K, V>& pair)
{
    os << "K: " << pair.first << ", V: " << pair.second << ";    ";
    return os;
}

template <typename K, typename V>
void CompleteBinaryTree<K, V>::preorderPrint(Node i) const
{
    if (i >= size)
    {
        return;
    }

    std::cout << N[i];
    preorderPrint(left(i));
    preorderPrint(right(i));
}

template <typename K, typename V>
void CompleteBinaryTree<K, V>::postorderPrint(Node i) const
{
    if (i >= size)
    {
        return;
    }

    postorderPrint(left(i));
    postorderPrint(right(i));
    std::cout << N[i];
}

template <typename K, typename V>
void CompleteBinaryTree<K, V>::swapNodes(Node i, Node j)
{
    std::pair<K, V> aux = N[j];
    N[j] = N[i];
    N[i] = aux;
}

template <typename K, typename V>
std::pair<K, V>& CompleteBinaryTree<K, V>::operator[](int pos)
{
    return N[pos];
}

template <typename K, typename V>
const std::pair<K, V>& CompleteBinaryTree<K, V>::operator[](int pos) const
{
    return N[pos];
}

template <typename K, typename V>
std::pair<K, V>& CompleteBinaryTree<K, V>::element(Node i)
{
    return this->operator[](i);
}

template <typename K, typename V>
const std::pair<K, V>& CompleteBinaryTree<K, V>::element(Node i) const
{
    return this->operator[](i);
}
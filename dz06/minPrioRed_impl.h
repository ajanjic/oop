template <typename K, typename V>
void MinPriorityQueue<K, V>::insert(const std::pair<K, V>& v)
{
    tree.N.push_back(v);
    upHeap(tree.size);
    
    tree.size++;
}

template <typename K, typename V>
void MinPriorityQueue<K, V>::upHeap(Node i)
{
    if(i == 0) return;
 
    Node parent = tree.parent(i);
    if (tree[parent].first > tree[i].first)
    {
        tree.swapNodes(i, parent);
        upHeap(parent);
    }
}

template <typename K, typename V>
void MinPriorityQueue<K, V>::downHeap(Node i)
{
    if (tree.isLeaf(i))
    {
        return;
    }

    Node left = tree.left(i);
    Node right = tree.right(i);
    if (right < tree.size)
    {
        if (tree[right] > tree[left])
        {
            if (tree[i].first >= tree[left].first)
            {
                tree.swapNodes(i, left);
                downHeap(left);
            }
            else if (tree[i].first >= tree[right].first)
            {
                tree.swapNodes(i, right);
                downHeap(right);
            }
        }
        else
        {
            if (tree[i].first >= tree[right].first)
            {
                tree.swapNodes(i, right);
                downHeap(right);
            }
            else if (tree[i].first >= tree[left].first)
            {
                tree.swapNodes(i, left);
                downHeap(left);
            }
        }
    }
    else
    {
        if (tree[i].first >= tree[left].first)
        {
            tree.swapNodes(i, left);
            downHeap(left);
        }
    }
}

template <typename K, typename V>
V MinPriorityQueue<K, V>::minimum() const
{
    return tree[0].second;
}

template <typename K, typename V>
V MinPriorityQueue<K, V>::extractMin()
{
    assert(!tree.empty() && "ERR: Tried to extract empty minimum priority queue.\n");

    V ret = minimum();
    tree.swapNodes(0, (tree.getSize() - 1));
    tree.N.pop_back();
    tree.size--;

    downHeap(0);

    return ret;
}

template <typename K, typename V>
void MinPriorityQueue<K, V>::buildMinHeap(const std::vector<std::pair<K, V>>& L)
{
    tree.N = L;
    tree.size = L.size();

    for (int i = tree.parent(tree.getSize()); i >= 0; i--)
    {
        downHeap((unsigned int) i);
    }
}

template <typename K, typename V>
void MinPriorityQueue<K, V>::decreaseKey(Node i, K newKey)
{
    assert(newKey < tree.N[i].first && "ERR: passed key is greater than already allocated key.\n");
    tree.N[i].first = newKey;
    upHeap(i);
}

template <typename K, typename V>
std::ostream& operator<<(std::ostream& os,const MinPriorityQueue<K, V>& obj)
{
    MinPriorityQueue<K,V> aux(obj);

    while(!aux.tree.empty())
    {
        os << aux.tree[0];
        aux.extractMin();
    }
    os << "\n";
    return os;

}
#include "minPrioRed.h"
#include<string>

int main()
{
    MinPriorityQueue<int, std::string> pq;
    pq.buildMinHeap({{1, "osoba4"}, {2, "osoba7"}, {7, "osoba5"}, {8, "osoba1"}, {5, "osoba6"}});
    std::cout << pq;

    pq.insert({6, "osoba9"});
    std::cout << pq;

    pq.extractMin();
    std::cout << pq;

    pq.decreaseKey(2, 4);
    std::cout << pq;
}
#include <iostream>
#include <list>
#include <string>
#include <utility>

using namespace std;
template <typename K, typename V>
struct MinPriorityQueue
{
    list<pair<K, V>> container;
    void insert(const pair<K, V>& el)
    {
        typename list<pair<K, V>>::iterator cur = container.begin();
        for (; cur != container.end(); cur++)
        {
            if (el.first < cur->first)
            {
                break;
            }
        }
        container.insert(cur, el);
    }
    V extractMin()
    {
        V ret = container.front().second;
        container.pop_front();
        return ret;
    }

    // pronalazimo element s kljucem key, te ga postavljamo
    // na odgovarajuće mjesto s obzirom na njegov novi kljuc
    // newKey
    void decreaseKey(const K& key, const K& newKey)
    {
        pair<K, V> elem;
        typename list<pair<K, V>>::iterator it = container.begin();
        for (; it != container.end(); it++)
        {
            if (it->first == key)
            {
                elem.second = it->second;
                elem.first = newKey;
                container.erase(it);
                break;
            }
        }
        insert(elem);
    }

    friend ostream& operator<<(ostream& os, const MinPriorityQueue<K, V>& obj)
    {
        for (const pair<K, V>& i : obj.container)
        {
            os << "K: " << i.first << ", V: " << i.second << ";\t";
        }
        os << "\n";
        return os;
    }
};

int main()
{
    MinPriorityQueue<int, string> minpq;
    minpq.insert({1, "osoba4"});
    minpq.insert({2, "osoba7"});
    minpq.insert({7, "osoba5"});
    minpq.insert({8, "osoba1"});
    minpq.insert({5, "osoba6"});
    cout << minpq << endl;

    minpq.insert({6, "osoba9"});
    cout << minpq << endl;

    minpq.extractMin();
    cout << minpq << endl;

    minpq.decreaseKey(6, 4);
    cout << minpq << endl;
}
#ifndef MINIMUMPRIORITYQUEUE_CBT
#define MINIMUMPRIORITYQUEUE_CBT
#include "cbt.h"
#include <cassert>

template <typename K, typename V>
class MinPriorityQueue
{
public:
    CompleteBinaryTree<K, V> tree;

    MinPriorityQueue() {}
    MinPriorityQueue(const MinPriorityQueue& src) : tree{src.tree} {}
    
    void downHeap(Node i);
    void upHeap(Node i);
    
    // s obzirom na prosljeđeni vektor, izgrađujemo prioritetni red
    // (pretpostavljamo da je prioritetni red prazan, ili pregazimo
    // sve dosad pohranjene parove)
    void buildMinHeap(const std::vector<std::pair<K, V>> & L);
    
    V minimum() const;
    V extractMin();
    
    // pronalazimo element s indeksom i, te ga postavljamo
    // na odgovarajuće mjesto s obzirom na njegov novi kljuc
    // newKey
    void decreaseKey(Node i, K newKey);

    void insert(const std::pair<K, V> & v);
};
#include "minPrioRed_impl.h"


#endif
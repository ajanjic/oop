#include "cbt.h"
#include <string>

int main()
{
    std::vector<std::pair<int, std::string>> L =
        {{0, "p"},{1, "pl"},{2, "pr"},{3, "pll"},{4, "plr"}};
    
    CompleteBinaryTree<int, std::string> cbt(L);
    cbt.preorderPrint(0u);
    std::cout << std::endl;
    cbt.swapNodes(0u, 4u);
    cbt.preorderPrint(0u);
    std::cout << std::endl;
    cbt.swapNodes(0u, 4u);

    cbt.updateRight(1u, {9, "up_plr"});
    cbt.preorderPrint(0u);
    std::cout << std::endl;

    cbt.postorderPrint(0u);
    std::cout << std::endl;

    cbt.preorderPrint(0u);
    std::cout << std::endl;
    cbt.postorderPrint(0u);
    std::cout << std::endl;

    std::cout << cbt.isLeaf(1u) << std::endl;
    std::cout << cbt.isLeaf(4u) << std::endl;
    std::cout << cbt.isLeaf(cbt.getSize()) << std::endl;

    std::cout << cbt.parent(0u) << std::endl; // :/
    std::cout << cbt.parent(1u) << std::endl; 
    std::cout << cbt.parent(2u) << std::endl; 
    std::cout << cbt.parent(3u) << std::endl; 
}
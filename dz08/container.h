#include <algorithm>
#include <cassert>
#include <iostream>
#include <utility>

template <typename T, size_t S>
class Container
{
protected:
    virtual T* first() = 0;
    virtual const T* first() const = 0;

public:
    Container() = default;
    virtual ~Container() = default;

    T* begin() { return first(); }
    T* end() { return first() + S; }
    const T* begin() const { return first(); }
    const T* end() const { return first() + S; }

    T& operator[](int i)
    {
        assert((size_t)i < S && "ERR: accessing out of bounds.\n");
        return *(first() + i);
    }
    const T& operator[](int i) const
    {
        assert((size_t)i < S && "ERR: accessing out of bounds.\n");
        return *(first() + i);
    }

    virtual void print(std::ostream& os) const = 0;
};

template <typename T, size_t S>
class StaticContainer : private Container<T, S>
{
private:
    T arr[S];

protected:
    T* first() override { return arr; }
    const T* first() const override { return arr; }

public:
    StaticContainer()
    {
        for (size_t i = 0; i < S; i++)
        {
            arr[i] = T();
        }
    }

    StaticContainer(std::initializer_list<T> lst)
    {
        std::copy(lst.begin(), lst.begin() + std::min(lst.size(), S), arr);
    }

    ~StaticContainer() override = default;

    void print(std::ostream& os) const override
    {
        os << "Static array: ";
        for (size_t i = 0; i < S; i++)
        {
            os << arr[i] << " ";
        }
        os << std::endl;
    }

    friend int main();  //srsly...
};

template <typename T, size_t S>
class DynamicContainer : private Container<T, S>
{
private:
    T* arr = nullptr;

protected:
    T* first() override { return arr; }
    const T* first() const override { return arr; }

public:
    DynamicContainer()
    {
        arr = new T[S];
        for (size_t i = 0; i < S; i++)
        {
            arr[i] = T();
        }
    }
    DynamicContainer(std::initializer_list<T> lst)
    {
        arr = new T[S];
        std::copy(lst.begin(), lst.begin() + std::min(lst.size(), S), arr);
    }
    ~DynamicContainer()
    {
        delete[] arr;
    }

    void print(std::ostream& os) const override
    {
        os << "Dynamic array: ";
        for (size_t i = 0; i < S; i++)
        {
            os << arr[i] << " ";
        }
        os << std::endl;
    }

    friend int main();
};
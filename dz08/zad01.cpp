constexpr double c_pi = 3.14159265358979323846;
#include <iostream>

class Shape
{
private:
public:
    virtual double area() const = 0; //PURE
    virtual ~Shape(){}
};

class Rectangle : public Shape
{
private:
    double width, height;

public:
    Rectangle(double width, double height) : width{width}, height{height} {}
    ~Rectangle(){}
    double area() const override
    {
        return width * height;
    }
};

class Circle : public Shape
{
private:
    double radius;

public:
    Circle(double radius) : radius{radius} {}
    ~Circle(){}
    double area() const override
    {
        return c_pi * radius * radius;
    }
};

int main()
{
    Shape* arr[6] = {new Rectangle(5.5, 7.2), new Rectangle(0.2, 1.5), new Rectangle(8.9, 9.8), new Circle(1.5), new Circle(2.6), new Circle(2.7)};

    double sum = 0.0;

    for (int i = 0; i < 6; i++)
    {
        sum += arr[i]->area();
    }

    std::cout << sum;

    for (int i = 0; i < 6; i++)
    {
        delete arr[i];
    }
}
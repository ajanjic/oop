#include "container.h"
#include <string>

int main()
{
    /*
    StaticContainer<int, 6> st1;
    StaticContainer<int, 6> st2{1, 0, 2, 7, 8, 1232};
    StaticContainer<int, 6> st3{1, 0, 3, 6, 9, 333};
    StaticContainer<int, 6> st4{1, 0, 4, 5, 10, 222};
    StaticContainer<int, 6> st5{1, 0, 4, 5, 10, 2229};
    DynamicContainer<int, 6> dy1{12, 22, 31, 40, 15, 16};
    DynamicContainer<int, 6> dy2{12, 20, 21, 70, 18, 11232};
    DynamicContainer<int, 6> dy3{12, 20, 31, 60, 19, 1333};
    DynamicContainer<int, 6> dy4{12, 20, 41, 50, 110, 1222};
    DynamicContainer<int, 6> dy5{12, 20, 41, 50, 110, 12229};
    

    StaticContainer<int, 6>  st1{1,1,1,1,1,1};
    StaticContainer<int, 6>  st2{1,1,1,1,1,1};
    StaticContainer<int, 6>  st3{1,1,1,1,1,1};
    StaticContainer<int, 6>  st4{1,1,1,1,1,1};
    StaticContainer<int, 6>  st5{1,1,1,1,1,1};
    DynamicContainer<int, 6> dy1{1,1,1,1,1,1};
    DynamicContainer<int, 6> dy2{1,1,1,1,1,1};
    DynamicContainer<int, 6> dy3{1,1,1,1,1,1};
    DynamicContainer<int, 6> dy4{1,1,1,1,1,1};
    DynamicContainer<int, 6> dy5{1,1,1,1,1,1};
    */

    StaticContainer<std::string, 3> st1{"a", "b", "c"};
    StaticContainer<std::string, 3> st2{"d", "e", "f"};
    StaticContainer<std::string, 3> st3{"g", "h", "i"};
    StaticContainer<std::string, 3> st4{"j", "k", "l"};
    StaticContainer<std::string, 3> st5{"m", "n", "o"};
    StaticContainer<std::string, 3> st6{"p", "q", "r"};
    DynamicContainer<std::string, 3> dy1{"s", "t", "u"};
    DynamicContainer<std::string, 3> dy2{"v", "w", "x"};
    DynamicContainer<std::string, 3> dy3{"y", "z", "1"};
    DynamicContainer<std::string, 3> dy4{"2", "3", "4"};
    DynamicContainer<std::string, 3> dy5{"5", "6", "7"};
    DynamicContainer<std::string, 3> dy6{"8", "9", "0"};

    //Container<int, 6>* arrs[] = {&st1, &st2, &st3, &st4, &st5, &dy1, &dy2, &dy3, &dy4, &dy5};
    Container<std::string, 3>* arrs[] = {&st1, &st2, &st3, &st4, &st5, &st6, &dy1, &dy2, &dy3, &dy4, &dy5, &dy6};

    std::cout << "Arrays:\n";
    for (auto& i : arrs)
    {
        i->print(std::cout);
    }

    //int sum = 0;
    std::string concat = "";

    for (auto& i : arrs)
    {
        for (const auto& j : (*i))
        {
            //sum += j;
            concat += j;
        }
    }

    //std::cout << "\nSum: " << sum << std::endl;
    std::cout << "\nConcat: " << concat << std::endl;
}

#include "accountClass.h"

LoginCredentials::LoginCredentials() : email{}, password{}, confirmPassword{} {}

LoginCredentials::LoginCredentials(const std::string& email, const std::string& password, const std::string& confirmPassword)
    : email{email}, password{password}, confirmPassword{confirmPassword} {}

bool LoginCredentials::validEmail() const
{
    return std::regex_match(email, std::regex(R"raw_str((\S+)@(\S+\..+))raw_str"));
    /*
    return std::regex_match(email, std::regex(R"raw_str((?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|
                                                        "(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09
                                                        \x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z
                                                        0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]
                                                        |[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|
                                                        [a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|
                                                        \\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))raw_str"));
    */
}

std::ostream& operator<<(std::ostream& os, const LoginCredentials& obj)
{
    os << "email: " << obj.email << "\nemail is valid: " << obj.validEmail() << "\npassword: " << obj.password << "\nconfirm password: " << obj.confirmPassword;
    return os;
}



Account::Account() {}

Account::Account(const std::string& username, const std::string& email, const std::string& password)
    : LoginCredentials(email, password, password), username{username} {}

std::string Account::getUsername() { return username; }
std::string Account::getEmail() { return email; }

std::ostream& operator<<(std::ostream& os, const Account& obj)
{
    os << "user: " << obj.username << "\n" << (LoginCredentials)obj;
    return os;
}
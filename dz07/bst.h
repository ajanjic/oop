#ifndef BINARY_SEARCH_TREE_IMPL
#define BINARY_SEARCH_TREE_IMPL

#ifdef _DEBUG
#include <fstream>
std::fstream newfile("new.dbg.txt", std::ios_base::trunc | std::ios_base::out);
std::fstream delfile("del.dbg.txt", std::ios_base::trunc | std::ios_base::out);
void* operator new(size_t size)
{
    void* p = malloc(size);
    newfile << p << "\n";
    return p;
}

void operator delete(void* p)
{
    delfile << p << "\n";
    free(p);
}
#endif

#include <iostream>
#include <vector>

template <typename K, typename V>
struct Node
{
    K key;
    V value;
    Node *parent, *left, *right;

    friend std::ostream& operator<<(std::ostream& os, const Node& n)
    {
        os << "Node at: " << &n
           << "; parent at: " << n.parent
           << "; left at: " << n.left
           << "; right at: " << n.right
           << "\nKey: " << n.key
           << "; value: " << n.value;
        return os;
    }

    Node() : key{}, value{}, parent{nullptr}, left{nullptr}, right{nullptr} {}
    Node(const Node<K, V>& src) : key{src.key}, value{src.value}, parent{nullptr}, left{nullptr}, right{nullptr} {}
    Node(const K& key, const V& val, Node<K, V>* parent) : key{key}, value{val}, parent{parent}, left{nullptr}, right{nullptr} {}
};

template <typename K, typename V>
struct BST
{
    Node<K, V>* root;

    BST();
    BST(const BST& src);
    ~BST();

    void copy(const Node<K, V>* src, Node<K, V>*& x, Node<K, V>* p);
    void clear(Node<K, V>* x);

    //pretrazuj podstablo s korijenom x dok ne pronadeš čvor
    //vrijednoscu key (u suprotnom vrati nullptr)
    Node<K, V>* search(Node<K, V>* x, K key);

    //vrati pokazivač na čvor koji ima minimalnu vrijednost
    //kljuca u podstablu čiji je korijen x
    Node<K, V>* minimum(Node<K, V>* x);

    //vrati pokazivač na čvor koji ima maksimalnu vrijednost
    //kljuca u podstablu čiji je korijen x
    Node<K, V>* maximum(Node<K, V>* x);

    //vrati sljedbenika čvora x po vrijednosti key unutar stabla
    Node<K, V>* successor(Node<K, V>* x);

    //vrati prethodnika čvora x po vrijednosti key unutar stabla
    Node<K, V>* predecessor(Node<K, V>* x);

    //unesi novi čvor brinuvši se o definicijibinary search tree-a
    void insert(const K& key, const V& value);

    //zamijeni podstabla s korijenima u i v
    void transplant(Node<K, V>* u, Node<K, V>* v);

    //obriši čvor x brinuvši se o definiciji binary search tree-a
    void remove(Node<K, V>* x);

    //napravi inorder obilazak, vrijednosti redom pohrani
    //u vektor nodes
    void inorderWalk(Node<K, V>* x, std::vector<Node<K, V>*>& nodes) const;

    //copy pridruživanje (pripaziti na shallow-copy)
    BST& operator=(const BST& bst);

    template <typename KK, typename VV>
    friend std::ostream& operator<<(std::ostream& os, const BST<KK, VV>& bst);
};
#include "bst_impl.h"

#endif
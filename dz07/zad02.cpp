#include "bst.h"
#include <string>

#ifdef _DEBUG
int main()
{
    BST<int, int> bst;
    bst.insert(4, 4);
    bst.insert(1, 1);
    bst.insert(2, 2);
    bst.insert(3, 3);
    bst.insert(5, 5);
    bst.insert(6, 6);
    bst.insert(7, 7);
    bst.insert(-10, -10);
    bst.insert(-20, -20);

    std::cout << bst;

    bst.remove(bst.root);
    bst.remove(bst.root);
    bst.remove(bst.root);
    std::cout << bst;

    std::cout << bst.successor(bst.root) << std::endl;
    std::cout << bst.predecessor(bst.root) << std::endl;
    std::cout << bst.maximum(bst.root) << std::endl;
    std::cout << bst.minimum(bst.root) << std::endl;

    BST<int, int> bst2;
    BST<int, int> bst3;

    bst2.insert(2,2);
    bst2.insert(3,3);
    std::cout << bst2;
    std::cout << bst3;
    bst3 = bst2 = bst;
    std::cout << bst2;
    std::cout << bst3;
}
#else
int main()
{
    BST<int, std::string> bst;
    bst.insert(20, "User1");
    bst.insert(25, "User2");
    bst.insert(15, "User3");
    bst.insert(17, "User4");

    std::cout << bst;

    bst.remove(bst.root);
    std::cout << bst;
}
#endif
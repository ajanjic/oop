template <typename K, typename V>
BST<K, V>::BST() : root{nullptr} {}

template <typename K, typename V>
void BST<K, V>::copy(const Node<K, V>* src, Node<K, V>*& x, Node<K, V>* p)
{
    if (src != nullptr)
    {
        x = new Node<K, V>(src->key, src->value, p);
        copy(src->left, x->left, x);
        copy(src->right, x->right, x);
    }
}

template <typename K, typename V>
BST<K, V>::BST(const BST<K, V>& src)
{
    if (src.root != nullptr)
    {
        copy(src.root, root, nullptr);
    }
    else
    {
        root = nullptr;
    }
}

template <typename K, typename V>
void BST<K, V>::clear(Node<K, V>* x)
{
    if (x != nullptr)
    {
        clear(x->left);
        clear(x->right);
        delete x;
    }
}

template <typename K, typename V>
BST<K, V>::~BST()
{
    clear(root);
}

template <typename K, typename V>
void BST<K, V>::inorderWalk(Node<K, V>* x, std::vector<Node<K, V>*>& nodes) const
{
    if (x != nullptr)
    {
        inorderWalk(x->left, nodes);
        nodes.push_back(x);
        inorderWalk(x->right, nodes);
    }
}

template <typename K, typename V>
void BST<K, V>::insert(const K& key, const V& value)
{
    Node<K, V>* aux = root;
    Node<K, V>* p = root;
    while (aux != nullptr)
    {
        p = aux;
        if (key < aux->key)
        {
            aux = aux->left;
        }
        else
        {
            aux = aux->right;
        }
    }
    aux = new Node<K, V>(key, value, p);
    if (p == nullptr)
    {
        root = aux;
    }
    else
    {
        if (key < p->key)
        {
            p->left = aux;
        }
        else
        {
            p->right = aux;
        }
    }
}

template <typename K, typename V>
Node<K, V>* BST<K, V>::search(Node<K, V>* x, K key)
{
    while (x != nullptr && key != x->key)
    {
        if (key < x->key)
        {
            x = x->left;
        }
        else
        {
            x = x->right;
        }
    }
    return x;
}

template <typename K, typename V>
Node<K, V>* BST<K, V>::minimum(Node<K, V>* x)
{
    while (x->left != nullptr)
    {
        x = x->left;
    }
    return x;
}

template <typename K, typename V>
Node<K, V>* BST<K, V>::maximum(Node<K, V>* x)
{
    while (x->right != nullptr)
    {
        x = x->right;
    }
    return x;
}

template <typename K, typename V>
Node<K, V>* BST<K, V>::successor(Node<K, V>* x)
{
    if (x->right != nullptr)
        return minimum(x->right);

    Node<K, V>* p = x->parent;
    while (p != nullptr && x == p->right)
    {
        x = p;
        p = p->parent;
    }
    return p;
}

template <typename K, typename V>
Node<K, V>* BST<K, V>::predecessor(Node<K, V>* x)
{
    if (x->left != nullptr)
        return maximum(x->left);

    Node<K, V>* p = x->parent;
    while (p != nullptr && x == p->left)
    {
        x = p;
        p = p->parent;
    }
    return p;
}

template <typename K, typename V>
void BST<K, V>::transplant(Node<K, V>* u, Node<K, V>* v)
{
    if (u->parent == nullptr)
    {
        root = v;
    }
    else
    {
        if (u == u->parent->left)
        {
            u->parent->left = v;
        }
        else
        {
            u->parent->right = v;
        }
    }

    if (v != nullptr)
    {
        v->parent = u->parent;
    }
}

template <typename K, typename V>
void BST<K, V>::remove(Node<K, V>* x)
{
    if (x->left == nullptr)
    {
        transplant(x, x->right);
    }
    else if (x->right == nullptr)
    {
        transplant(x, x->left);
    }
    else
    {
        Node<K, V>* y = minimum(x->right);
        if (y->parent != x)
        {
            transplant(y, y->right);
            y->right = x->right;
            y->right->parent = y;
        }
        transplant(x, y);
        y->left = x->left;
        y->left->parent = y;
    }
    delete x;
}

template <typename K, typename V>
BST<K, V>& BST<K, V>::operator=(const BST& bst)
{
    clear(root);
    if (bst.root != nullptr)
    {
        //root = new Node<K, V>(*(bst.root));
        copy(bst.root, root, nullptr);
    }
    else
    {
        root = nullptr;
    }
    return *this;
}

template <typename K, typename V>
std::ostream& operator<<(std::ostream& os, const BST<K, V>& bst)
{
    os << "-------------------------------------------------------\nBinary search tree with root at: ";
    os << bst.root << "\nNodes:\n";

    std::vector<Node<K, V>*> nodes;
    bst.inorderWalk(bst.root, nodes);

    for (unsigned int i = 0; i < nodes.size(); i++)
    {
        os << (*nodes[i]) << "\n";
    }
    os << "-------------------------------------------------------" << std::endl;
    return os;
}
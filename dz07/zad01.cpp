#include "accountClass.h"
#include <iostream>
//requires: accountClass.cpp

int main()
{
    LoginCredentials lc[] = {
        {"example@example.com", "password", "password"},
        {"exam ple@example.com", "password", "password"},
        {"exa.mple@example.com", "password", "password"},
        {"@example.com", "password", "password"},
        {"example@.com", "password", "password"}};

    for (int i = 0; i < 5; i++)
    {
        std::cout << i << ":\n"
                  << lc[i] << std::endl
                  << std::endl;
    }

    std::cout << std::endl;
    Account acc("user", "mail@example.com", "password");
    std::cout << acc;
}
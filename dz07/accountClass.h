#ifndef ACCOUNTLOGIN_IMPLEMENTATION
#define ACCOUNTLOGIN_IMPLEMENTATION

#include <iostream>
#include <regex>
#include <string>

class LoginCredentials
{
private:
    std::string confirmPassword;

protected:
    std::string email;
    std::string password;

public:
    LoginCredentials();
    /*prosljeđujemo email, password i confirm password*/
    LoginCredentials(const std::string& email, const std::string& password, const std::string& confirmPassword);

    bool validEmail() const;

    friend std::ostream& operator<<(std::ostream& os, const LoginCredentials& obj);
};

class Account : public LoginCredentials
{
protected:
    std::string username;

public:
    Account();
    /*prosljeđujemo username, email i password*/
    Account(const std::string& username, const std::string& email, const std::string& password);

    std::string getUsername();
    std::string getEmail();

    friend std::ostream& operator<<(std::ostream& os, const Account& obj);
};
#endif
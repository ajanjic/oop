#include "dynamicqueue.h"

DynamicQueue::DynamicQueue() : container{} {}
DynamicQueue::DynamicQueue(const DynamicQueue &q) : container{q.container} {}
DynamicQueue::~DynamicQueue() {}

bool DynamicQueue::empty() const { return container.empty(); }

void DynamicQueue::enqueue(double x)
{
    container.append(x);
}

double DynamicQueue::dequeue()
{
    return container.removeFromHead();
}

void DynamicQueue::print() const
{
    container.print();
}
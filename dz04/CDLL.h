#ifndef CDLL_HEADER
#define CDLL_HEADER
#include <iostream>
#include <cassert>

struct Node
{
    Node *prev;
    Node *next;
    double value;

    Node();
    Node(double value);
    Node(const Node &n);
    // prilikom čišćenja ispisujemo adresu
    ~Node();

    //swap premješta sadržaj između dva čvora
    void swap(Node &n);

    //ispisujemo adresu, adresu prethodnog, adresu sljedećeg te vrijednost
    void print() const;
};

class CDLL
{
    //public:
protected:
    Node *head;
    Node *tail;

public:
    CDLL();
    CDLL(const CDLL &c);
    ~CDLL();

    bool empty() const;

    // postavi vrijednost na početak
    void prepend(double value);

    //postavi vrijednost na kraj
    void append(double value);

    //ukloni čvor s početka
    //i vrati njegovu vrijednost
    double removeFromHead();

    //ukloni čvor s kraja
    //i vrati njegovu vrijednost
    double removeFromTail();

    //ispisujemo adresu head-a, tail-a te sve čvorove
    void print() const;

    void sort();
};
#endif
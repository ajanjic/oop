#include "dynamicqueue.h"
//requires compilation: CSLL.cpp, dynamicqueue.cpp

int main()
{
    double load[7] = {59.9, 13.7, 10.0, 98.44, 16.7, 20.269, 1.5};

    DynamicQueue *q = new DynamicQueue;
    q->print();
    for (int i = 0; i < 7; i++)
    {
        q->enqueue(load[i]);
    }
    q->print();

    DynamicQueue *qtwo = new DynamicQueue(*q);
    qtwo->print();
    delete q;
    delete qtwo;
}
#include "CSLL.h"

Node::Node() : next{nullptr}, value{} {}
Node::Node(double v) : next{nullptr}, value{v} {}
Node::Node(const Node &n) : next{n.next}, value{n.value} {}
Node::~Node()
{
    std::cout << "Node " << this << " deleted." << std::endl;
}

void Node::print() const
{
    std::cout << "Node at: " << this
              << "; next at: " << next
              << "; value: " << value
              << std::endl;
}

CSLL::CSLL() : tail{nullptr} {}
CSLL::CSLL(const CSLL &c) : tail{nullptr}
{
    if (!c.empty())
    {
        Node *it = c.tail->next;
        do
        {
            append(it->value);
            it = it->next;
        } while (it != c.tail->next);
    }
}
CSLL::~CSLL()
{
    if (!empty())
    {
        Node *aux = tail->next;
        Node *it = tail->next;
        while (it != tail)
        {
            it = it->next;
            delete aux;
            aux = it;
        }
        delete aux;
        std::cout << "---------------------------------------------------\n---------------------------------------------------" << std::endl;
    }
}

bool CSLL::empty() const
{
    return tail == nullptr;
}

void CSLL::prepend(double value)
{
    Node *n = new Node(value);
    if (empty())
    {
        tail = n;
        n->next = n;
    }
    else
    {
        n->next = tail->next;
        tail->next = n;
    }
}

void CSLL::append(double value)
{
    prepend(value);
    tail = tail->next;
}

void CSLL::print() const
{
    std::cout << "Tail at: " << tail << std::endl;
    if (!empty())
    {
        std::cout << "...................................................\n";
        Node *it = tail->next;
        for (; it != tail; it = it->next)
        {
            it->print();
        }
        it->print();
    }
    std::cout << "---------------------------------------------------\n---------------------------------------------------" << std::endl;
}

double CSLL::removeFromHead()
{
    if (empty())
    {
        assert(0 && "Trying to remove element from empty list");
        return 0.0;
    }

    double ret = tail->next->value;

    if (tail->next == tail)
    {
        delete tail;
        tail = nullptr;

        return ret;
    }

    Node *aux = tail->next;
    tail->next = tail->next->next;

    delete aux;
    return ret;
}

double CSLL::removeFromTail()
{
    if (empty())
    {
        assert(0 && "Trying to remove element from empty list");
        return 0.0;
    }

    double ret = tail->value;

    if (tail->next == tail)
    {
        delete tail;
        tail = nullptr;

        return ret;
    }

    Node *aux = tail;
    Node *it = tail;

    while (it->next != tail)
    {
        it = it->next;
    }
    if (it = tail)
        it->next = tail->next;
    tail = it;

    delete aux;
    return ret;
}
#include "CDLL.h"

Node::Node() : prev{nullptr}, next{nullptr}, value{0} {}
Node::Node(double value) : prev{nullptr}, next{nullptr}, value{value} {}
Node::Node(const Node &n) : prev{n.prev}, next{n.next}, value{n.value} {}

Node::~Node()
{
    std::cout << "Node " << this << " deleted." << std::endl;
}

void Node::swap(Node &n)
{
    double aux = n.value;
    n.value = value;
    value = aux;
}

void Node::print() const
{
    std::cout << "Node at: " << this
              << "; prev at: " << prev
              << "; next at: " << next
              << "; value: " << value
              << std::endl;
}

CDLL::CDLL() : head{nullptr}, tail{nullptr} {}
CDLL::CDLL(const CDLL &c) : head{nullptr}, tail{nullptr}
{
    if (!c.empty())
    {
        Node *it = c.head;

        do
        {
            append(it->value);
            it = it->next;
        } while (it != c.head);
    }
}
CDLL::~CDLL()
{
    if (!empty())
    {
        Node *it = head;
        do
        {
            it = it->next;
            delete it->prev;
        } while (it != tail);
        delete tail;
    }
    std::cout << "---------------------------------------------------\n---------------------------------------------------" << std::endl;
}

bool CDLL::empty() const
{
    return head == nullptr;
}

void CDLL::prepend(double value)
{
    Node *aux = new Node(value);
    if (empty())
    {
        aux->next = aux;
        aux->prev = aux;
        head = aux;
        tail = aux;
    }
    else
    {
        aux->prev = tail;
        tail->next = aux;

        aux->next = head;
        head->prev = aux;
        head = aux;
    }
}

void CDLL::append(double value)
{
    Node *aux = new Node(value);
    if (empty())
    {
        aux->next = aux;
        aux->prev = aux;
        head = aux;
        tail = aux;
    }
    else
    {
        aux->prev = tail;
        tail->next = aux;

        aux->next = head;
        head->prev = aux;
        tail = aux;
    }
}

double CDLL::removeFromHead()
{
    if (empty())
    {
        assert(0 && "Trying to remove element from empty list");
        return 0.0;
    }

    double val = head->value;
    if (head == tail)
    {
        delete head;
        head = nullptr;
        tail = nullptr;
    }
    else
    {
        tail->next = head->next;
        head->next->prev = tail;
        delete head;
        head = tail->next;
    }
    return val;
}

double CDLL::removeFromTail()
{
    if (empty())
    {
        assert(0 && "Trying to remove element from empty list");
        return 0.0;
    }

    double val = tail->value;
    if (head == tail)
    {
        delete tail;
        tail = nullptr;
        head = nullptr;
    }
    else
    {
        head->prev = tail->prev;
        tail->prev->next = head;
        delete tail;
        tail = head->prev;
    }
    return val;
}

void CDLL::sort()
{
    if (!empty())
    {
        for (Node *i = head->next; i != head; i = i->next)
        {
            for (Node *j = tail; j != i->prev; j = j->prev)
            {
                if (j->value < j->prev->value)
                {
                    j->swap(*j->prev);
                }
            }
        }
    }
}

void CDLL::print() const
{
    std::cout << "Head at: " << head << "; tail at: " << tail << std::endl;
    if (!empty())
    {
        std::cout << "...................................................\n";
        Node *it = head;
        for (it; it != tail; it = it->next)
        {
            it->print();
        }
        it->print();
    }
    std::cout << "---------------------------------------------------\n---------------------------------------------------" << std::endl;
}
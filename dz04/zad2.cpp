#include "CDLL.h"
//requires compilation: CDLL.cpp

int main()
{
    double load[7] = {59.9, 13.7, 10.0, 98.44, 16.7, 20.269, 1.5};

    CDLL *lstFoo = new CDLL;
    lstFoo->print();
    for (int i = 0; i < 7; i++)
    {
        lstFoo->append(load[i]);
    }
    lstFoo->print();

    CDLL *lstBar = new CDLL(*lstFoo);
    lstBar->sort();
    lstBar->print();

    delete lstFoo;
    delete lstBar;
}
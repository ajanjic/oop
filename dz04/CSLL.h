#ifndef CSLL_HEADER
#define CSLL_HEADER
#include <iostream>
#include <cassert>

struct Node
{
    Node *next;
    double value;

    Node();
    Node(double value);
    Node(const Node &n);
    ~Node();

    void print() const;
};

class CSLL
{
protected:
    Node *tail;

public:
    CSLL();
    CSLL(const CSLL &c);
    ~CSLL();

    bool empty() const;
    void prepend(double value);
    void append(double value);

    double removeFromHead();
    double removeFromTail();
    void print() const;
};
#endif
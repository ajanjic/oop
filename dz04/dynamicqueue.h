#include "CSLL.h"
//requires compilation: CSLL.cpp

class DynamicQueue
{
protected:
    CSLL container;

public:
    DynamicQueue();
    DynamicQueue(const DynamicQueue &q);
    ~DynamicQueue();

    bool empty() const;
    void enqueue(double x);
    double dequeue();
    void print() const;
};
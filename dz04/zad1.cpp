#include <iostream>
#include <list>

double sum(std::list<double> &lst)
{
    double ret = 0.0;
    for (std::list<double>::iterator it = lst.begin(); it != lst.end(); it++)
    {
        ret += *it;
    }
    return ret;
}

double prod(std::list<double> &lst)
{
    double ret = 1.0;
    for (std::list<double>::iterator it = lst.begin(); it != lst.end(); it++)
    {
        ret *= *it;
    }
    return ret;
}

double min(std::list<double> &lst)
{
    std::list<double>::iterator it = lst.begin();
    double ret = *(it++);
    for (; it != lst.end(); it++)
    {
        if (*it < ret)
        {
            ret = *it;
        }
    }
    return ret;
}

double max(std::list<double> &lst)
{
    std::list<double>::iterator it = lst.begin();
    double ret = *(it++);
    for (; it != lst.end(); it++)
    {
        if (*it > ret)
        {
            ret = *it;
        }
    }
    return ret;
}

int main()
{
    std::list<double> lst = {59.9, 13.7, 10.0, 98.44, 16.7, 20.269, 1.5};

    std::cout << "Sum: " << sum(lst) << std::endl;
    std::cout << "Product: " << prod(lst) << std::endl;
    std::cout << "Mininum: " << min(lst) << std::endl;
    std::cout << "Maximum: " << max(lst) << std::endl;
}
#include "myvec.h"
#include "mystack.h"

int main()
{
    //
    //ZAD1
    //
    std::cout << "ZAD1:\n\n";

    MyVector *vec1 = new MyVector;
    for (int i = 1; i < 11; i++)
    {
        vec1->pushBack(i);
    }
    std::cout << "Vector 1: ";
    vec1->print();

    MyVector *vec2 = new MyVector(*vec1);
    delete vec1;
    std::cout << "Vector 2: ";
    vec2->print();
    do
    {
        vec2->popBack();
        std::cout << "          ";
        vec2->print();
    } while (!vec2->empty());

    delete vec2;
    //
    //ZAD2
    //
    std::cout << "ZAD2:\n\n";

    DynamicStack *stack1 = new DynamicStack;
    for (int i = 1; i < 11; i++)
    {
        stack1->push(i);
    }
    std::cout << "Stack 1: ";
    stack1->print();

    DynamicStack *stack2 = new DynamicStack(*stack1);
    delete stack1;
    std::cout << "Stack 2: ";
    stack2->print();
    do
    {
        stack2->pop();
        std::cout << "         ";
        stack2->print();
    } while (!stack2->empty());

    delete stack2;
}
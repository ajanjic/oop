#pragma once
#include <iostream>
#include <vector>

class DynamicStack
{
protected:
    std::vector<int> container;

public:
    DynamicStack() {}
    DynamicStack(const DynamicStack &S) : container{S.container} {}
    bool empty() const { return container.empty(); }
    void push(int x) { container.push_back(x); }
    int pop();
    void print();
};

int DynamicStack::pop()
{
    int ret = container.back();
    container.pop_back();
    return ret;
}

void DynamicStack::print()
{
    for (int i = 0; i < container.size(); i++)
    {
        std::cout << container[i] << " ";
    }
    std::cout << std::endl;
}
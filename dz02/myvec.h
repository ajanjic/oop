#pragma once
#include <iostream>
#include <cstring>
#include <cassert>

class MyVector
{
protected:
    unsigned int _size, _capacity;
    int *P;

    void resize(unsigned int size);

public:
    MyVector();
    MyVector(const MyVector &);
    ~MyVector();
    void pushBack(int);
    int popBack();
    unsigned int getSize() const { return _size; }
    unsigned int getCapacity() const { return _capacity; }
    void print();
    bool empty() const { return _size == 0; }
    bool full() const { return _size == _capacity; }
    int &at(unsigned int pos);
    int &front() { return at(0); }
    int &back() { return at(_size - 1); }
};

MyVector::MyVector()
    : _size{0}, _capacity{0}, P{nullptr}
{
}

MyVector::MyVector(const MyVector &src)
    : _size{src._size}, _capacity{src._size}
{
    P = new int[_size];
    memcpy(P, src.P, _size * sizeof(int));
}

MyVector::~MyVector()
{
    if (P != nullptr)
    {
        delete[] P;
    }
}

void MyVector::resize(unsigned int size)
{
    if (size == 0)
    {
        if (P != nullptr)
        {
            delete[] P;
            P = nullptr;
        }
    }
    else
    {
        int *newdata = new int[size];
        if (P != nullptr)
        {
            memcpy(newdata, P, _size * sizeof(int));
            delete[] P;
        }
        P = newdata;
    }
    _capacity = size;
}

void MyVector::pushBack(int val)
{
    if (full())
    {
        if (_capacity == 0)
        {
            resize(1);
        }
        else
        {
            resize(_capacity * 2);
        }
    }
    P[_size++] = val;
}

int MyVector::popBack()
{
    assert(!empty() && "ERR: Tried to pop empty vector.");

    int ret = P[--_size];
    if (_size * 2 <= _capacity)
    {
        resize(_size);
    }

    return ret;
}

int &MyVector::at(unsigned int pos)
{
    assert(pos < _size && "ERR: Out of bounds access.");

    return P[pos];
}

void MyVector::print()
{
    for (int i = 0; i < _size; i++)
    {
        std::cout << P[i] << " ";
    }
    std::cout << std::endl;
}
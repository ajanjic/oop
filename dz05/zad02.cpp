#include <iostream>
#include "sqrmat.h"

int main()
{
    SquareMatrix mat(10);
    for(int i = 0; i < 10; i++)
    {
        mat[i][i] = (i + 1.0);
    }

    SquareMatrix matOne(10);
    matOne += 1.0;

    mat += matOne;
    //std::cout.precision(2);
    //std::cout << std::fixed << mat << std::endl;
    std::cout << mat << std::endl;

    SquareMatrix mat2(0);

    mat2 = mat;

    mat2 /= 10.0;

    std::cout << mat2;
}
#ifndef MY_VEC_IMPL
#define MY_VEC_IMPL
#include <iostream>
#include <cstring>
#include <cassert>

class MyVector
{
//protected:
public:
    unsigned int _size, _capacity;
    int* P;

    void resize(unsigned int size);

public:
    MyVector();
    MyVector(const MyVector &);
    ~MyVector();

    void pushBack(int);
    void popBack();

    unsigned int getSize() const;
    unsigned int getCapacity() const;

    bool empty() const;
    bool full() const;

    int& at(unsigned int pos);
    int& front();
    int& back();

    //operator pridruživanja
    MyVector& operator=(const MyVector& oth);

    /*
    Povećaj element na itom mjestu za vrijednost
    koja se nalazi na itom mjestu vektora
    prosljeđenog po referenci
    */
    MyVector& operator+=(const MyVector& oth);
    
    //zbroj vektora
    MyVector operator+(const MyVector& oth) const;
    
    //skalarni produkt vektora
    int operator*(const MyVector& oth)const;
    
    //provjera jesu li dva vektora jednaki po elementima
    bool operator==(const MyVector& oth) const;
    
    //provjera jesu li dva vektora različiti po elementima
    bool operator!=(const MyVector& oth) const;
    
    //operatori dohvaćanja
    const int& operator[](int pos) const;
    int& operator[](int pos);
    
    /*
    početak i kraj vektora
    (obratite pažnju što treba vraćati end)
    */
    int* begin() const;
    int* end() const;
};
#endif
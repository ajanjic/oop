#include <iostream>
#include "myvec.h"

void print(MyVector& vec)
{
    for(auto& a : vec)
    {
        std::cout << a << " ";
    }
    std::cout << std::endl;
}

int main()
{
    MyVector vec;
    std::cout << "Before populating\nVec begin: " << vec.begin() << " vec end: " << vec.end() << std::endl;

    for (int i = 0; i < 25; i += 2)
    {
        vec.pushBack(i);
    }
    std::cout << "After populating\nVec begin: " << vec.begin() << " vec end: " << vec.end() << std::endl << std::endl;

    std::cout << "vec: ";
    print(vec);

    std::cout << "(vec)^2 = " << vec * vec << std::endl;

    MyVector vecSum = vec + vec;
    std::cout << "(vec + vec): ";
    print(vecSum);

    MyVector vec3;
    vec3 = vecSum;
    vec3 = vec;
    vec3 = vecSum + vec;
    std::cout << "vec3: ";
    print(vec3);

    std::cout << "vec3 == vec: " << (vec3 == vec) << std::endl;
    std::cout << "vec == vec: " << (vec == vec) << std::endl;

}
#include "sqrmat.h"

SquareMatrix::SquareMatrix(unsigned int val) : M{val}
{
    if(M == 0)
    {
        container = nullptr;
    }
    else
    {
        container = new double*[M];
        for(int i = 0; i < M; i++)
        {
            container[i] = new double[M]{0.0};
        }
    }
}

SquareMatrix::SquareMatrix(const SquareMatrix& src)
{
    M = src.M;

    if(M == 0)
    {
        container = nullptr;
    }
    else
    {
        container = new double*[M];
        for(int i = 0; i < M; i++)
        {
            container[i] = new double[M];
        }

        for(int i = 0; i < M; i++)
        {
            for(int j = 0; j < M; j++)
            {
                container[i][j] = src.container[i][j];
            }
        }
    }
}

SquareMatrix::~SquareMatrix()
{
    if(container != nullptr)
    {
        for(int i = 0; i < M; i++)
        {
            delete[] container[i];
        }
        delete[] container;
    }
}

double* SquareMatrix::operator[](unsigned int row)
{
    assert(row < M && "ERR: Out of bound access.\n");
    return container[row];
}
const double* SquareMatrix::operator[](unsigned int row) const
{
    assert(row < M && "ERR: Out of bound access.\n");
    return container[row];
}

SquareMatrix& SquareMatrix::operator=(const SquareMatrix& oth)
{
    if(container != nullptr)
    {
        for(int i = 0; i < M; i++)
        {
            delete[] container[i];
        }
        delete[] container;
    }

    M = oth.M;
    container = new double*[M];
    for(int i = 0; i < M; i++)
    {
        container[i] = new double[M];
    }

    for(int i = 0; i < M; i++)
    {
        for(int j = 0; j < M; j++)
        {
            container[i][j] = oth.container[i][j];
        }
    }

    return *this;
}

SquareMatrix& SquareMatrix::operator=(double oth)
{
    for(int i = 0; i < M; i++)
    {
        for(int j = 0; j < M; j++)
        {
            container[i][j] = oth;
        }
    }

    return *this;
}

SquareMatrix& SquareMatrix::operator+=(const SquareMatrix& oth)
{
    assert(oth.M == M && "ERR: Cannot add two matricies of different dimensions.\n");

    for(int i = 0; i < M; i++)
    {
        for(int j = 0; j < M; j++)
        {
            container[i][j] += oth.container[i][j];
        }
    }

    return *this;
}

SquareMatrix& SquareMatrix::operator-=(const SquareMatrix& oth)
{
    assert(oth.M == M && "ERR: Cannot subtract two matricies of different dimensions.\n");

    for(int i = 0; i < M; i++)
    {
        for(int j = 0; j < M; j++)
        {
            container[i][j] -= oth.container[i][j];
        }
    }

    return *this;
}

SquareMatrix& SquareMatrix::operator*=(const SquareMatrix& oth)
{
    assert(oth.M == M && "ERR: Cannot multiply two matricies of different dimensions.\n");

    SquareMatrix aux(M);

    for(int i = 0; i < M; i++)
    {
        for(int j = 0; j < M; j++)
        {
            for(int k = 0; k < M; k++)
            {
                aux.container[i][j] += container[i][k] * oth.container[k][j];
            }
        }
    }
    *this = aux;

    return *this;
}

SquareMatrix& SquareMatrix::operator+=(double oth)
{
    for(int i = 0; i < M; i++)
    {
        for(int j = 0; j < M; j++)
        {
            container[i][j] += oth;
        }
    }

    return *this;
}

SquareMatrix& SquareMatrix::operator-=(double oth)
{
    for(int i = 0; i < M; i++)
    {
        for(int j = 0; j < M; j++)
        {
            container[i][j] -= oth;
        }
    }

    return *this;
}
SquareMatrix& SquareMatrix::operator*=(double oth)
{
    for(int i = 0; i < M; i++)
    {
        for(int j = 0; j < M; j++)
        {
            container[i][j] *= oth;
        }
    }

    return *this;
}

SquareMatrix& SquareMatrix::operator/=(double oth)
{
    //assert(oth != 0.0 && "ERR: Division by 0.\n");
    //assert(oth != INFINITY && "ERR: Division by infinity.\n");

    for(int i = 0; i < M; i++)
    {
        for(int j = 0; j < M; j++)
        {
            container[i][j] /= oth;
        }
    }

    return *this;
}
SquareMatrix SquareMatrix::operator+(const SquareMatrix& oth) const
{
    SquareMatrix aux(*this);
    aux += oth;
    return aux;
}

SquareMatrix SquareMatrix::operator-(const SquareMatrix& oth) const
{
    SquareMatrix aux(*this);
    aux -= oth;
    return aux;
}

SquareMatrix SquareMatrix::operator*(const SquareMatrix& oth)const
{

    SquareMatrix aux(*this);
    aux *= oth;
    return aux;
}

SquareMatrix SquareMatrix::operator+(double oth) const
{
    SquareMatrix aux(*this);
    aux += oth;
    return aux;
}

SquareMatrix SquareMatrix::operator-(double oth) const
{
    SquareMatrix aux(*this);
    aux -= oth;
    return aux;
}

SquareMatrix SquareMatrix::operator*(double oth) const
{
    SquareMatrix aux(*this);
    aux *= oth;
    return aux;
}

SquareMatrix SquareMatrix::operator/(double oth) const
{
    SquareMatrix aux(*this);
    aux /= oth;
    return aux;
}

bool  SquareMatrix::operator==(const SquareMatrix& oth) const
{
    if(M != oth.M) return false;

    for(int i = 0; i < M; i++)
    {
        for(int j = 0; j < M; j++)
        {
            if(container[i][j] != oth.container[i][j]) return false;
        }
    }

    return true;
}

bool  SquareMatrix::operator!=(const SquareMatrix& oth) const
{
    return !(operator==(oth));
}

std::ostream& operator<<(std::ostream& out, const SquareMatrix& obj)
{
    for(int i = 0; i < obj.M; i++)
    {
        for(int j = 0; j < obj.M; j++)
        {
            out << obj[i][j] << " ";
        }
        out << std::endl;
    }

    return out;
}
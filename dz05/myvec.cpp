#include "myvec.h"

unsigned int min(unsigned int a, unsigned int b)
{
    return (a < b ? a : b);
}

MyVector::MyVector()
    : _size{0}, _capacity{0}, P{nullptr} {}

MyVector::MyVector(const MyVector& src)
    : _size{src._size}, _capacity{src._size}
{
    P = new int[_size];
    memcpy(P, src.P, _size * sizeof(int));
}

MyVector::~MyVector()
{
    if (P != nullptr)
    {
        delete[] P;
    }
}

void MyVector::resize(unsigned int size)
{
    if (size == 0)
    {
        if (P != nullptr)
        {
            delete[] P;
            P = nullptr;
        }
    }
    else
    {
        int *newdata = new int[size];
        if (P != nullptr)
        {
            memcpy(newdata, P, _size * sizeof(int));
            delete[] P;
        }
        P = newdata;
    }
    _capacity = size;
}

void MyVector::pushBack(int val)
{
    if (full())
    {
        if (_capacity == 0)
        {
            resize(1);
        }
        else
        {
            resize(_capacity * 2);
        }
    }
    P[_size++] = val;
}

void MyVector::popBack()
{
    assert(!empty() && "ERR: Tried to pop empty vector.\n");

    --_size;
    if (_size * 2 <= _capacity)
    {
        resize(_size);
    }
}

bool MyVector::empty() const { return _size == 0; }
bool MyVector::full() const { return _size == _capacity; }

int& MyVector::at(unsigned int pos)
{
    assert(pos < _size && "ERR: Out of bounds access.\n");

    return P[pos];
}

int& MyVector::front()
{
    assert(!empty() && "ERR: No member to access for int& MyVector::front().\n");
    return at(0);
}

int& MyVector::back()
{
    assert(!empty() && "ERR: No member to access for int& MyVector::back().\n");
    return at(_size - 1);
}

MyVector& MyVector::operator=(const MyVector& oth)
{
    _size = oth._size;
    _capacity = oth._size;

    delete[] P;
    P = new int[_size];
    
    memcpy(P, oth.P, _size * sizeof(int));

    return *this;
}

MyVector& MyVector::operator+=(const MyVector& oth)
{
    unsigned int minIndex = min(oth._size, _size);

    for(int i = 0; i < minIndex; i++)
    {
        P[i] += oth.P[i];
    }

    return *this;
}

MyVector MyVector::operator+(const MyVector& oth) const
{
    MyVector aux(*this);
    return aux += oth;
}

int MyVector::operator*(const MyVector& oth) const
{
    assert(_size == oth._size && "ERR: Unable to do dot product on two vectors of different sizes.\n");
    
    int acc = 0;

    for(int i = 0; i < _size; i++)
    {
        acc += P[i] * oth.P[i];
    }

    return acc;
}

bool MyVector::operator==(const MyVector& oth) const
{
    if(_size != oth._size) return false;

    for(int i = 0; i < _size; i++)
    {
        if(P[i] != oth.P[i]) return false;
    }

    return true;
}

bool MyVector::operator!=(const MyVector& oth) const
{
    return !(operator==(oth));
}

const int& MyVector::operator[](int pos) const
{
    assert(pos < _size && "ERR: Out of bounds access.\n");

    return P[pos];
}

int& MyVector::operator[](int pos) { return at(pos); }

int* MyVector::begin() const
{
    if(empty()) return nullptr;

    return &P[0];
}
int* MyVector::end() const
{
    if(empty()) return nullptr;


    return ((&P[_size - 1]) + 1);
}
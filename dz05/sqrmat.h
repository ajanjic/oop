#ifndef SQUARE_MATRIX_IMPL
#define SQUARE_MATRIX_IMPL
#include <iostream>
#include <cassert>
#include <cmath>


class SquareMatrix
{
public: 
    /*
    pokazivač na dimanički alocirano polje duljine M čiji su elementi
    pokazivači na dinamički alocirana polja duljine M (retci u matrici)
    */
    double** container;
    unsigned int M;

    SquareMatrix() = delete;
    //inicijaliziraj sve ćelije na 0.0
    SquareMatrix(unsigned int val);
    SquareMatrix(const SquareMatrix& src);
    ~SquareMatrix();
    
    double* operator[](unsigned int row);
    const double* operator[](unsigned int row) const;
    
    SquareMatrix& operator=(const SquareMatrix &);
    
    SquareMatrix& operator+=(const SquareMatrix& oth);
    SquareMatrix& operator-=(const SquareMatrix& oth);
    SquareMatrix& operator*=(const SquareMatrix& oth);
    // postavi sve ćelije na isti skalar
    SquareMatrix& operator=(double oth);
    //svim ćelijama nadodaj isti skalar
    SquareMatrix& operator+=(double oth);
    // od svih ćelija oduzmi isti skalar
    SquareMatrix& operator-=(double oth);
    // svaku ćeliju pomnoži sa skalarom
    SquareMatrix& operator*=(double oth);
    // svaku ćeliju podijeli sa skalarom
    SquareMatrix& operator/=(double oth);
    
    SquareMatrix operator+(const SquareMatrix& oth) const;
    SquareMatrix operator-(const SquareMatrix& oth) const;
    SquareMatrix operator*(const SquareMatrix& oth) const;
    SquareMatrix operator+(double oth) const;
    SquareMatrix operator-(double oth) const;
    SquareMatrix operator*(double oth) const;
    SquareMatrix operator/(double oth) const;
    
    bool operator==(const SquareMatrix& oth) const;
    bool operator!=(const SquareMatrix& oth) const;

    friend std::ostream& operator<<(std::ostream& out, const SquareMatrix& obj);
};
#endif
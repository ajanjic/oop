#include <iostream>
#include <algorithm>
#include <numeric>
#include <fstream>

struct Student
{
    int scores[5];
    Student() : scores{0, 0, 0, 0, 0} {}
    void fillScores(int (&arr)[5])
    {
        for (int i = 0; i < 5; i++)
        {
            scores[i] = arr[i];
        }
    }
    int calculateTotalScore() const
    {
        return std::accumulate(scores, scores + 5, 0);
    }
    void print() const
    {
        for (int i = 0; i < 5; i++)
        {
            std::cout << scores[i] << " ";
        }
        std::cout << std::endl;
    }
};

int main()
{
    std::cout << "Zadatak 2:\n";
    std::fstream file("data.txt", std::ios::in);
    int n;
    file >> n;

    Student *arr = new Student[n];

    for (int i = 0; i < n; i++)
    {
        int aux[5];
        for (int j = 0; j < 5; j++)
        {
            file >> aux[j];
        }
        arr[i].fillScores(aux);
        //arr[i].print();
    }
    file.close();

    int scoreToBeat = arr[0].calculateTotalScore();
    std::cout << std::count_if(arr + 1, arr + n, [&](Student &a) { return a.calculateTotalScore() >= scoreToBeat; });

    delete[] arr;
}
#include <iostream>
#include <algorithm>
#include <cmath>

struct PositionVector
{
    double x, y;
    PositionVector() : x{0}, y{0} {}
    PositionVector(double x, double y) : x{x}, y{y} {}
    PositionVector(const PositionVector &src) : x{src.x}, y{src.y} {}
    double norm() const { return sqrt(x * x + y * y); }
    void print() const
    {
        std::cout << " (" << x << ", " << y << ") \t||(" << x << ", " << y << ")|| = " << norm() << std::endl;
    }
};

int main()
{

    PositionVector arr[] = {{2.5, 3.6}, {5.5, 3.6}, {4.4, 4.4}, {10.0, 0.1}, {0.0, 0.0}};

    std::cout << "Zadatak 1:\nNon-sorted: \n";
    for (PositionVector &i : arr)
    {
        i.print();
    }

    std::cout << "\nSorted: \n";
    std::sort(arr, arr + sizeof(arr) / sizeof(PositionVector), [](PositionVector &a, PositionVector &b) { return a.norm() > b.norm(); });

    for (PositionVector &i : arr)
    {
        i.print();
    }
}
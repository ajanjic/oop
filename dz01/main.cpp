#include <iostream>
#include <random>
#include <cstdlib>

class Triangle;

class Point
{
public:
    double x, y;

    Point() : x{0.0}, y{0.0} {}
    Point(double a, double b) : x{a}, y{b} {}
    ~Point() = default;

    void print();
    bool isInsideTriangle(const Triangle &);

    Point operator-(const Point &b) const
    {
        return {x - b.x, y - b.y};
    }
};

class Triangle
{
public:
    Point A, B, C;

    Triangle(const Point &a, const Point &b, const Point &c) : A{a}, B{b}, C{c} {}
    ~Triangle() = default;
    void print();
};

void Point::print()
{
    std::cout << "(" << x << ", " << y << ")";
}

int crossk(Point va, Point vb)
{
    double val = (va.x * vb.y - va.y * vb.x);
    if (val == 0)
    {
        return 0;
    }
    else
    {
        return (val < 0 ? -1 : 1);
    }
}

bool Point::isInsideTriangle(const Triangle &trig)
{
    Point AB = trig.B - trig.A;
    Point BC = trig.C - trig.B;
    Point CA = trig.A - trig.C;
    Point AP = *this - trig.A;
    Point BP = *this - trig.B;
    Point CP = *this - trig.C;

    int ABP = crossk(AB, AP);
    int BCP = crossk(BC, BP);
    int CAP = crossk(CA, CP);

    bool k1 = ABP == 0 || ABP == crossk(AB, BC);
    bool k2 = BCP == 0 || BCP == crossk(BC, CA);
    bool k3 = CAP == 0 || CAP == crossk(CA, AB);

    return (ABP == 0 || ABP == crossk(AB, BC)) &&
           (BCP == 0 || BCP == crossk(BC, CA)) &&
           (CAP == 0 || CAP == crossk(CA, AB));
}

void Triangle::print()
{
    std::cout << "A = ";
    A.print();
    std::cout << ", ";
    std::cout << "B = ";
    B.print();
    std::cout << ", ";
    std::cout << "C = ";
    C.print();
}

int main()
{
    //TESTCASE 1
    Point **pOnes = new Point *[9];
    pOnes[0] = new Point(0, 0);
    pOnes[1] = new Point(0, 3);
    pOnes[2] = new Point(2, 2);
    pOnes[3] = new Point(1, 1.5);
    pOnes[4] = new Point(1, 0.5);
    pOnes[5] = new Point(1, 1);
    pOnes[6] = new Point(1, 1.000000001);
    pOnes[7] = new Point(1, 0.999999999);
    pOnes[8] = new Point(0, 0);

    Triangle *trigOne = new Triangle(*pOnes[0], *pOnes[1], *pOnes[2]);

    std::cout << "Test case 1:\n";
    std::cout << "Trokut: ";
    trigOne->print();
    std::cout << std::endl;

    for (int i = 3; i < 9; i++)
    {
        //windows really doesn't like non ascii in cmd
        //std::cout << "Točka ";
        std::cout << "Tocka ";
        pOnes[i]->print();
        std::cout << " se" << (pOnes[i]->isInsideTriangle(*trigOne) ? " " : " ne ") << "nalazi u trokutu ABC." << std::endl;
    }

    for (int i = 0; i < 9; i++)
    {
        delete pOnes[i];
    }
    delete pOnes;
    delete trigOne;

    //TESTCASE 2
    std::cout << std::endl
              << std::endl
              << "Test case 2:\n";

    double minRand = -150;
    double maxRand = 150;
    std::random_device rd;
    std::default_random_engine generator = std::default_random_engine(rd());
    std::uniform_real_distribution<double> distrib(minRand, maxRand);

    Point *pTwo = new Point();
    Triangle *trigTwo = new Triangle({}, {}, {});
    for (int i = 0; i < 10; i++)
    {
        trigTwo->A.x = distrib(generator);
        trigTwo->A.y = distrib(generator);
        trigTwo->B.x = distrib(generator);
        trigTwo->B.y = distrib(generator);
        trigTwo->C.x = distrib(generator);
        trigTwo->C.y = distrib(generator);

        do
        {
            pTwo->x = distrib(generator);
            pTwo->y = distrib(generator);
        } while (!pTwo->isInsideTriangle(*trigTwo));

        pTwo->print();
        std::cout << " je unutar ABC, ";
        trigTwo->print();
        std::cout << std::endl;
    }

    delete pTwo;
    delete trigTwo;
}
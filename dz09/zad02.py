from vehicle import Vehicle 
from car import Car 
from motorcycle import Motorcycle 


if __name__ == "__main__":
    electricBike = Vehicle(2018, 0)
    electricBike.drive(47)
    electricBike.drive(35)

    print('\n')

    car1 = Car(2011, 100453)
    bike1 = Motorcycle(2011, 5002)

    car1.drive(220)
    print('\n')
    
    bike1.drive(5)
    bike1.ride_on_the_rear_wheel()
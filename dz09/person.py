class Person(object):
    __slots__ = 'name', 'age'

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def walk(self):
        print("{} ({}) walks".format(self.name, self.age))
    
    def say(self, message):
        print("{} says: {}".format(self.name, message))
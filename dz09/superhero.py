from person import Person

class Superhero(Person):
    __slots__ = 'secret_identity'

    def __init__(self, name, age, secret_identity):
        super().__init__(name, age)
        self.secret_identity = secret_identity

    def fly(self):
        print("{} ({}) (aka {}) flies".format(self.name, self.age, self.secret_identity))

    def walk(self, use_power):
        if use_power:
            print("{} (aka {}) uses extra-speed running".format(self.name, self.secret_identity))
        else:
            super().walk()

    def use_super_sight(self):
        print("{} (aka {}) uses super sight".format(self.name, self.secret_identity))

    
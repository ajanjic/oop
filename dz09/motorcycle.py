from vehicle import Vehicle 

class Motorcycle(Vehicle):
    def ride_on_the_rear_wheel(self):
        print("Motorcycle rides on the rear wheel.")
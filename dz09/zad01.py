from person import Person 
from superhero import Superhero 

if __name__ == "__main__":
    Osoba1 = Person('Mike', 31)
    Osoba1.walk()
    Osoba1.say("Hello World!")
    Osoba2 = Superhero("Wade Winston Wilson", 33, 'Deadpool')

    print('\n')

    Osoba2.walk(0)
    Osoba2.say("Goodbye World!")
    Osoba2.use_super_sight()
    Osoba2.walk(1)
    Osoba2.fly()
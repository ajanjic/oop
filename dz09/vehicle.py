class Vehicle(object):
    __slots__ = 'model_year', 'kilometers'

    def __init__(self, year, tot_km):
        self.model_year = year
        self.kilometers = tot_km

    def drive(self, new_kilometers):
        self.kilometers = self.kilometers + new_kilometers
        print("After {} kilometers, the odometer reading is {}".format(new_kilometers, self.kilometers))